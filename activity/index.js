// console.log('hello world');
// S24 Activity Template:
/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

// Code here:
const number = 2;
const getCube = 2 ** 3;
console.log(`the cube of ${number} is ${getCube}`);

/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/

// Code here:
const address = [
  "2112",
  "New York St",
  "Don Cornelio Subd.",
  "Mabiga Mabalacat",
  "Pampanga",
  "2010",
];
const [houseNumber, street, barangay, town, city, zip] = address;

console.log(
  `I live at ${houseNumber}, ${street}, ${barangay}, ${town} ${city}, ${zip} `
);

/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
// Code here:
const animal = {
  name: "Lolong",
  type: "saltwater",
  family: "crocodile",
  weight: 1075,
  measurement: [20, 3],
};

const { name, type, family, weight, measurement } = animal;
const [ft, inch] = measurement;

console.log(
  `${name} was a ${type} ${family}. He weighed at ${weight} kgs with a measurement of ${ft} ft ${inch} in.`
);

/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

// Code here:
const numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => console.log(number));
/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

// Code here:
class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const myDog = new Dog("Lucy", 6, "American Bully");
console.log(myDog);
